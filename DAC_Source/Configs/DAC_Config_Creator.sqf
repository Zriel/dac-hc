//////////////////////////////
//    Dynamic-AI-Creator    //
//    Version 3.1b - 2014   //
//--------------------------//
//    DAC_Config_Creator    //
//--------------------------//
//    Script by Silola      //
//    silola@freenet.de     //
//////////////////////////////

scalar = "any";DAC_Init_Camps = 0;

waituntil{missionNamespace getVariable ["BIS_fnc_init",false]};

if(isAIcontroller) then {if(local player) then {DAC_Code = 1} else {DAC_Code = 0}} else {if(isnull player) then {DAC_Code = 3} else {DAC_Code = 2}};

//===============|
// DAC_Settings	 |
//===============|=============================================================================================|

	if(isNil "DAC_STRPlayers")	then {		DAC_STRPlayers		= ["BLU_ST1_CM1","BLU_ST1_CM2",
										"BLU_ST1_A_L1","BLU_ST1_A_L2",
										"BLU_ST1_A_1_1","BLU_ST1_A_1_2","BLU_ST1_A_1_3","BLU_ST1_A_1_4","BLU_ST1_A_1_5","BLU_ST1_A_1_6",
										"BLU_ST1_A_2_1","BLU_ST1_A_2_2","BLU_ST1_A_2_3","BLU_ST1_A_2_4","BLU_ST1_A_2_5","BLU_ST1_A_2_6",
										"BLU_ST1_B_L1","BLU_ST1_B_L2",
										"BLU_ST1_B_1_1","BLU_ST1_B_1_2","BLU_ST1_B_1_3","BLU_ST1_B_1_4","BLU_ST1_B_1_5","BLU_ST1_B_1_6",
										"BLU_ST1_B_2_1","BLU_ST1_B_2_2","BLU_ST1_B_2_3","BLU_ST1_B_2_4","BLU_ST1_B_2_5","BLU_ST1_B_2_6",
										"BLU_ST1_Sierra1","BLU_ST1_Sierra2","BLU_ST1_Sierra3","BLU_ST1_Sierra4",

										"OPF_ST1_CM1","OPF_ST1_CM2",
										"OPF_ST1_A_L1","OPF_ST1_A_L2",
										"OPF_ST1_A_1_1","OPF_ST1_A_1_2","OPF_ST1_A_1_3","OPF_ST1_A_1_4","OPF_ST1_A_1_5","OPF_ST1_A_1_6",
										"OPF_ST1_A_2_1","OPF_ST1_A_2_2","OPF_ST1_A_2_3","OPF_ST1_A_2_4","OPF_ST1_A_2_5","OPF_ST1_A_2_6",
										"OPF_ST1_B_L1","OPF_ST1_B_L2",
										"OPF_ST1_B_1_1","OPF_ST1_B_1_2","OPF_ST1_B_1_3","OPF_ST1_B_1_4","OPF_ST1_B_1_5","OPF_ST1_B_1_6",
										"OPF_ST1_B_2_1","OPF_ST1_B_2_2","OPF_ST1_B_2_3","OPF_ST1_B_2_4","OPF_ST1_B_2_5","OPF_ST1_B_2_6",
										"OPF_ST1_Sierra1","OPF_ST1_Sierra2","OPF_ST1_Sierra3","OPF_ST1_Sierra4",

										"RES_ST1_CM1","RES_ST1_CM2",
										"RES_ST1_A_L1","RES_ST1_A_L2",
										"RES_ST1_A_1_1","RES_ST1_A_1_2","RES_ST1_A_1_3","RES_ST1_A_1_4","RES_ST1_A_1_5","RES_ST1_A_1_6",
										"RES_ST1_A_2_1","RES_ST1_A_2_2","RES_ST1_A_2_3","RES_ST1_A_2_4","RES_ST1_A_2_5","RES_ST1_A_2_6",
										"RES_ST1_B_L1","RES_ST1_B_L2",
										"RES_ST1_B_1_1","RES_ST1_B_1_2","RES_ST1_B_1_3","RES_ST1_B_1_4","RES_ST1_B_1_5","RES_ST1_B_1_6",
										"RES_ST1_B_2_1","RES_ST1_B_2_2","RES_ST1_B_2_3","RES_ST1_B_2_4","RES_ST1_B_2_5","RES_ST1_B_2_6",
										"RES_ST1_Sierra1","RES_ST1_Sierra2","RES_ST1_Sierra3","RES_ST1_Sierra4",

										"i1","i2","i3","i4","i5","i6","i7","i8","i9","i10","i11","i12","i13",
										"zap1","zap2","zap3","zap4","zap5","zap6","zap7","zap8","zap9",
										"e1","e2","e3","e4","e5","e6","e7","e8","e9"]};

	if(isNil "DAC_AI_Count_Level")	then {		DAC_AI_Count_Level 	= [[2,4],[3,6],[4,8],[6,12],[1,0]]		};
	if(isNil "DAC_Dyn_Weather") 	then {		DAC_Dyn_Weather		= [0,0,0,[0, 0, 0],0]					};
	if(isNil "DAC_Reduce_Value") 	then {		DAC_Reduce_Value	= [900,1100,0.1]						};
	if(isNil "DAC_AI_Spawn") 		then {		DAC_AI_Spawn		= [[10,5,5],[10,5,15],0,120,250,1]		};
	if(isNil "DAC_Delete_Value") 	then {		DAC_Delete_Value 	= [[180,300],[180,500],6000]	 		};
	if(isNil "DAC_Del_PlayerBody") 	then {		DAC_Del_PlayerBody	= [0,0]									};
	if(isNil "DAC_Com_Values") 		then {		DAC_Com_Values		= [1,2,0,0]								};
	if(isNil "DAC_AI_AddOn") 		then {		DAC_AI_AddOn		= 1										};
	if(isNil "DAC_AI_Level") 		then {		DAC_AI_Level		= 3										};
	if(isNil "DAC_Res_Side") 		then {		DAC_Res_Side		= 2										};
	if(isNil "DAC_Marker") 			then {		DAC_Marker			= 0										};
	if(isNil "DAC_WP_Speed") 		then {		DAC_WP_Speed		= 0.01									};
	if(isNil "DAC_Join_Action")		then {		DAC_Join_Action		= false									};
	if(isNil "DAC_Fast_Init") 		then {		DAC_Fast_Init		= false									};
	if(isNil "DAC_Player_Marker")	then {		DAC_Player_Marker	= false									};
	if(isNil "DAC_Direct_Start")	then {		DAC_Direct_Start	= false									};
	if(isNil "DAC_Activate_Sound")	then {		DAC_Activate_Sound	= true									};
	if(isNil "DAC_Auto_UnitCount")	then {		DAC_Auto_UnitCount	= [15,10]								};
	if(isNil "DAC_Player_Support")	then {		DAC_Player_Support	= [10,[4,2000,3,1000]]					};
	if(isNil "DAC_SaveDistance")	then {		DAC_SaveDistance	= [500,["DAC_Save_Pos"]]				};
	if(isNil "DAC_Radio_Max")		then {		DAC_Radio_Max		= DAC_AI_Level							};

	DAC_BadBuildings 	= [];
	DAC_GunNotAllowed	= [];
	DAC_VehNotAllowed	= [];
	DAC_Locked_Veh		= [];
	DAC_SP_Soldiers     	= [//VANILLA
				"B_soldier_AR_F","B_G_soldier_AR_F","O_soldier_AR_F","O_soldierU_AR_F","O_G_soldier_AR_F","I_soldier_AR_F","I_G_soldier_AR_F",
				//Leights OPFOR
				"LOP_AM_Infantry_AR","LOP_AFR_Infantry_AR","LOP_ISTS_Infantry_AR","LOP_RACS_Infantry_MG","LOP_IA_Infantry_MG","LOP_CDF_Infantry_MG","LOP_AA_Infantry_MG","LOP_AA_Police_MG","LOP_ChDKZ_Infantry_MG","LOP_SLA_Infantry_MG","LOP_US_Infantry_MG",
				//RHS
				"rhs_msv_machinegunner","rhsusf_army_ocp_machinegunner","rhs_vdv_machinegunner","rhsusf_usmc_marpat_wd_machinegunner","rhsusf_usmc_marpat_wd_autorifleman_m249","rhsusf_usmc_marpat_wd_autorifleman_m249_ass",
				//ST1 PMC
				"ST1_PMC_Soldier_AR"];
	DAC_Data_Array 		= [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,[]];
	DAC_Marker_Val		= [];
	DAC_Zones			= [];

	//=============================================================================================================|

	_scr = [] spawn (compile preprocessFile "\DAC_Source\Scripts\DAC_Start_Creator.sqf");
	waituntil {scriptdone _scr};
	sleep 0.1;
	waituntil {(DAC_Basic_Value > 0)};

if(DAC_Code < 2) then
{
	//===========================================|
	// StartScriptOnServer                       |
	//===========================================|
	//player sidechat "ServerStart"
	//[] execVM "myServerScript.sqf";
	//onMapSingleClick "_fun = [_pos,_shift]execVM ""Action.sqf""";
}
else
{
	if(DAC_Code == 3) then
	{
		//===========================================|
		// StartScriptOnJipClient                    |
		//===========================================|
		//player sidechat "JipClientStart"
		//[] execVM "myJipClientScript.sqf";
	}
	else
	{
		//===========================================|
		// StartScriptOnClient                       |
		//===========================================|
		//player sidechat "ClientStart"
		//[] execVM "myClientScript.sqf";
		//onMapSingleClick "_fun = [_pos,_shift]execVM ""Action.sqf""";
	};
};